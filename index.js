function countLetter(letter, sentence) {
    let result = 0;
     if (typeof letter !== 'string' || letter.length !== 1) {
        return undefined;
    }

    // Convert the letter and sentence to lowercase
    const lowercaseLetter = letter.toLowerCase();
    const lowercaseSentence = sentence.toLowerCase();

    let count = 0;
    // Loop through the sentence and count occurrences of the letter
    for (let i = 0; i < lowercaseSentence.length; i++) {
        if (lowercaseSentence.charAt(i) === lowercaseLetter) {
            count++;
        }
    }

    return count;
    // Check first whether the letter is a single character.
    // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
    // If letter is invalid, return undefined.

    
}


function isIsogram(text) {
    const lowerCaseWord = text.toLowerCase();

  // Create a set to store unique characters
  const charSet = new Set();

  // Loop through each character in the word
  for (const char of lowerCaseWord) {
    // Check if the character is already in the set
    if (charSet.has(char)) {
      // If the character is repeating, return false
      return false;
    }
    // Add the character to the set
    charSet.add(char);
  }

  // If the loop completes without finding any repeating characters, return true
  return true;

    // An isogram is a word where there are no repeating letters.
    // The function should disregard text casing before doing anything else.
    // If the function finds a repeating letter, return false. Otherwise, return true.
    
}

function purchase(age, price) {
  if (age < 13) {
    return undefined;
  } else if (age >= 13 && age <= 21 || age >= 65) {
    // 20% discount for students aged 13 to 21 and senior citizens.
    const discountedPrice = 0.8 * price;
    return discountedPrice.toFixed(2);
  } else {
    // Rounded off price for people aged 22 to 64.
    const roundedOffPrice = price;
    return roundedOffPrice.toFixed(2);
  }


    // Return undefined for people aged below 13.
    // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
    // Return the rounded off price for people aged 22 to 64.
    // The returned value should be a string.
    
}

function findHotCategories(items) {

    const hotCategories = items.filter(item => item.stocks === 0);
    const uniqueHotCategories = [...new Set(hotCategories.map(item => item.category))];

    return uniqueHotCategories;
}
const items = [
    { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' },
    { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' },
    { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' },
    { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' },
    { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }
];

    // Find categories that has no more stocks.
    // The hot categories must be unique; no repeating categories.

    // The passed items array from the test are the following:
    // { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
    // { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
    // { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
    // { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
    // { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }

    // The expected output after processing the items array is ['toiletries', 'gadgets'].
    // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.



function findFlyingVoters(candidateA, candidateB) {

    
    const flyingVoters = candidateA.filter(voter => candidateB.includes(voter));
    return flyingVoters;
}

const candidateA = ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m'];
const candidateB = ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l'];
    
    // Find voters who voted for both candidate A and candidate B.

    // The passed values from the test are the following:
    // candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
    // candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']

    // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].
    // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.
    


module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};